// ALiu Edit: The class Link of link.h is renamed as Link1
//            The class LList of llist.h is renamed as LList1
// Shaffer's Source code: link.h llist.h


#ifndef b03_LIST2_H
#define	b03_LIST2_H
#include "a01_book.h"
#include "b01_ListADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Singly Linked-List ----------------------------------------------------------
 * Time Complexity
 * insert:      Theta(1)
 * remove:      Theta(1)
 * next:        Theta(1)
 * prev:        Theta(n), average and worst
 * moveToPos:   Theta(n), average and worst; Theta(i), for the i-th position
 * The remaining operations: Theta(1)
 * Space Complexity
 * : Theta(n)
 */

// Singly linked list node
template <typename E> class Link1 {
public:
  E element;      // Value for this node
  Link1 *next;        // Pointer to next node in list
  // Constructors
  Link1(const E& elemval, Link1* nextval =NULL)
    { element = elemval;  next = nextval; }
  Link1(Link1* nextval =NULL) { next = nextval; }
};


// This is the declaration for LList1. It is split into two parts
// because it is too big to fit on one book page
// Linked list implementation
template <typename E> class LList1: public List<E> {
private:
  Link1<E>* head;       // Pointer to list header
  Link1<E>* tail;       // Pointer to last element
  Link1<E>* curr;       // Access to current element
  int cnt;    	       // Size of list

  void init() {        // Intialization helper method
    curr = tail = head = new Link1<E>;
    cnt = 0;
  }

  void removeall() {   // Return link nodes to free store
    while(head != NULL) {
      curr = head;
      head = head->next;
      delete curr;
    }
  }

public:
  LList1(int size=defaultSize) { init(); }    // Constructor
  ~LList1() { removeall(); }                   // Destructor
  void print() const;                // Print list contents
  void clear() { removeall(); init(); }       // Clear list

  // Insert "it" at current position
  void insert(const E& it) {
    curr->next = new Link1<E>(it, curr->next);  
    if (tail == curr) tail = curr->next;  // New tail
    cnt++;
  }

  void append(const E& it) { // Append "it" to list
    tail = tail->next = new Link1<E>(it, NULL);
    cnt++;
  }

  // Remove and return current element
  E remove() {
    Assert(curr->next != NULL, "No element");
    E it = curr->next->element;      // Remember value
    Link1<E>* ltemp = curr->next;     // Remember link node
    if (tail == curr->next) tail = curr; // Reset tail
    curr->next = curr->next->next;   // Remove from list
    delete ltemp;                    // Reclaim space
    cnt--;			     // Decrement the count
    return it;
  }

  void moveToStart() // Place curr at list start
    { curr = head; }

  void moveToEnd()   // Place curr at list end
    { curr = tail; }

  // Move curr one step left; no change if already at front
  void prev() {
    if (curr == head) return;       // No previous element
    Link1<E>* temp = head;
    // March down list until we find the previous element
    while (temp->next!=curr) temp=temp->next;
    curr = temp;
  }

  // Move curr one step right; no change if already at end
  void next()
    { if (curr != tail) curr = curr->next; }

  int length() const  { return cnt; } // Return length

  // Return the position of the current element
  int currPos() const {
    Link1<E>* temp = head;
    int i;
    for (i=0; curr != temp; i++)
      temp = temp->next;
    return i;
  }

  // Move down list to "pos" position
  void moveToPos(int pos) {
    Assert ((pos>=0)&&(pos<=cnt), "Position out of range");
    curr = head;
    for(int i=0; i<pos; i++) curr = curr->next;
  }

  const E& getValue() const { // Return current element
    Assert(curr->next != NULL, "No value");
    return curr->next->element;
  }
};

#endif	/* b03_LIST2_H */

