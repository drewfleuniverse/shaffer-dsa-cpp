// ALiu Edit: In class SAList, keyword using is add to explicitly exposed 
//            member functions inherited from class AList.
// Shaffer's Source code: alist.h salist.h


#ifndef b02_LIST1_H
#define	b02_LIST1_H
#include "a01_book.h"
#include "a03_KVpair.h"
#include "b01_ListADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Array-Based List ------------------------------------------------------------
 * Time Complexity
 * moveToPos:   Theta(1)
 * getValue:    Theta(1)
 * append:      Theta(1)
 * insert:      Theta(n), average and worst
 * remove:      Theta(n), average and worst
 * ctor:        more than const time
 * dtor:        more than const time
 * clear:       more than const time
 * The remaining operations: Theta(1)
 * Space Complexity
 * : Omega(n)
 */

// This is the declaration for AList. It is split into two parts
// because it is too big to fit on one book page
template <typename E> // Array-based list implementation
class AList : public List<E> {
private:
  int maxSize;        // Maximum size of list
  int listSize;       // Number of list items now
  int curr;           // Position of current element
  E* listArray;    // Array holding list elements

public:
  AList(int size=defaultSize) { // Constructor
    maxSize = size;
    listSize = curr = 0;
    listArray = new E[maxSize];
  }

  ~AList() { delete [] listArray; } // Destructor

  void clear() {                    // Reinitialize the list
    delete [] listArray;            // Remove the array
    listSize = curr = 0;            // Reset the size
    listArray = new E[maxSize];  // Recreate array
  }

  // Insert "it" at current position
  void insert(const E& it) {
    Assert(listSize < maxSize, "List capacity exceeded");
    for(int i=listSize; i>curr; i--)  // Shift elements up
      listArray[i] = listArray[i-1];  //   to make room
    listArray[curr] = it;
    listSize++;                       // Increment list size
  }

  void append(const E& it) {       // Append "it"
    Assert(listSize < maxSize, "List capacity exceeded");
    listArray[listSize++] = it;
  }

  // Remove and return the current element.
  E remove() {
    Assert((curr>=0) && (curr < listSize), "No element");
    E it = listArray[curr];           // Copy the element
    for(int i=curr; i<listSize-1; i++)  // Shift them down
      listArray[i] = listArray[i+1];
    listSize--;                          // Decrement size
    return it;
  }
  void moveToStart() { curr = 0; }        // Reset position
  void moveToEnd() { curr = listSize; }     // Set at end
  void prev() { if (curr != 0) curr--; }       // Back up
  void next() { if (curr < listSize) curr++; } // Next

  // Return list size
  int length() const  { return listSize; }

  // Return current position
  int currPos() const { return curr; }

  // Set current list position to "pos"
  void moveToPos(int pos) {
    Assert ((pos>=0)&&(pos<=listSize), "Pos out of range");
    curr = pos;
  }

  const E& getValue() const { // Return current element
    Assert((curr>=0)&&(curr<listSize),"No current element");
    return listArray[curr];
  }
};


/* Sorted Array-Based List -----------------------------------------------------
 * 
 */

// Sorted array-based list
// Inherit from AList as a protected base class
template <typename Key, typename E>
class SAList: protected AList<KVpair<Key,E> > {
public:
  SAList(int size=defaultSize) :
    AList<KVpair<Key,E> >(size) {}

  ~SAList() {}                    // Destructor

  // Redefine insert function to keep values sorted
  void insert(KVpair<Key,E>& it) { // Insert at right
    KVpair<Key,E> curr;
    for (moveToStart(); currPos() < length(); next()) {
      curr = getValue();
      if(curr.key() > it.key())
        break;
    }
    AList<KVpair<Key,E> >::insert(it); // Do AList insert
  }

  // With the exception of append, all remaining methods are
  // exposed from AList. Append is not available to SAlist
  // class users since it has not been explicitly exposed.
  using AList<KVpair<Key,E> >::clear;
  using AList<KVpair<Key,E> >::remove;
  using AList<KVpair<Key,E> >::moveToStart;
  using AList<KVpair<Key,E> >::moveToEnd;
  using AList<KVpair<Key,E> >::prev;
  using AList<KVpair<Key,E> >::next;
  using AList<KVpair<Key,E> >::length;
  using AList<KVpair<Key,E> >::currPos;
  using AList<KVpair<Key,E> >::moveToPos;
  using AList<KVpair<Key,E> >::getValue;
};

#endif	/* b02_LIST1_H */

