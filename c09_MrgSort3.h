// ALiu Edit: The template functions below are merged into a template class
//            MrgSort3.
//            The function main is removed.
// Shaffer's Source code: mrgsort3.cpp


#ifndef b09_MRGSORT3_H
#define	b09_MRGSORT3_H
#include "a01_book.h"
#include "c01_SortADT.h"
extern int THRESHOLD;
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.

// ALiu Edited: the template functions below are rewrote into template class

template <typename E, typename Comp>
class MrgSort3 : public SortADT<E, Comp> {
public:
  // Mergesort implementation optimized to reverse the 2nd half,
  // so that there is no need to test for exhausted sublists.
  // Also, sublists of length <= THRESHOLD are sorted with insertion sort.

  // Standard insertion sort implementation
  void inssort3(E A[], int n) { // Insertion Sort
    for (int i=1; i<n; i++)       // Insert i'th record
      for (int j=i; (j>0) && (Comp::prior(A[j], A[j-1])); j--)
        swap(A, j, j-1);
  }

  //Optimized mergesort implementation
  void mergesort3(E A[], E temp[], int left, int right) {
    if ((right-left) <= THRESHOLD) { // Small list
      inssort3(&A[left], right-left+1);
      return;
    }
    int i, j, k, mid = (left+right)/2;
    mergesort3(A, temp, left, mid);
    mergesort3(A, temp, mid+1, right);
    // Do the merge operation.  First, copy 2 halves to temp.
    for (i=mid; i>=left; i--) temp[i] = A[i];
    for (j=1; j<=right-mid; j++) temp[right-j+1] = A[j+mid];
    // Merge sublists back to A
    for (i=left,j=right,k=left; k<=right; k++)
      if (Comp::prior(temp[i], temp[j])) A[k] = temp[i++];
      else A[k] = temp[j--];
  }

  void sort(E* array, int n) {
    static E* temp = NULL;
    if (temp == NULL) temp = new E[n];  // Declare temp array
    mergesort3(array, temp, 0, n-1);
  }
};

#endif	/* b09_MRGSORT3_H */

