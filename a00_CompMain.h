// ALiu Edit: The function main from comptest is slightly modified and renamed
//            as Comptest
// Shaffer's Source code: comptest.h


#ifndef a00_COMPMAIN_H
#define	a00_COMPMAIN_H
#include "a01_book.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.

// A timing test to see how much overhead is caused by using comparators
// We time three version: a comparator classe for ints, 
// a comparator class for ints with the function members declared inline,
// and raw comparison of ints (no comparator class)

// Define a standard comparator class for ints
class intintCompare_ {
public:
  static bool lt(int x, int y) { return x < y; }
  static bool eq(int x, int y) { return x == y; }
  static bool gt(int x, int y) { return x > y; }
};

// Same as intintCompare_ in book.h, but inline the functions
class iiiComp {
public:
  inline static bool lt(int x, int y) { return x < y; }
  inline static bool eq(int x, int y) { return x == y; }
  inline static bool gt(int x, int y) { return x > y; }
};

void CompTest(int numruns) {
  int a = 10;
  int b = 20;
  bool dum;

  int i;

  //  For each test, we do many calls to the comparator within
  // the loop to minimize the overhead costs.

  Settime();
  for(i=0; i<numruns; i++) {
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
    dum = intintCompare_::lt(a, b);
  }
  cout << "Time for class comparison: "
	   << Gettime() << " sec\n";

  Settime();
  for(i=0; i<numruns; i++) {
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
    dum = iiiComp::lt(a, b);
  }
  cout << "Time for inline'd class comparison: "
	   << Gettime() << " sec\n";

  Settime();
  for (i=0; i<numruns; i++) {
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
    dum = a < b;
  }
  cout << "Time for simple comparison: "
       << Gettime() << " sec\n";
}

#endif	/* a00_COMPMAIN_H */

