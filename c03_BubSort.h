// ALiu Edit: The template functions below are merged into template class
//            BubSort.
//            The function main is removed.
// Shaffer's Source code: bubsort.cpp


#ifndef b03_BUBSORT_H
#define	b03_BUBSORT_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Bubble Sort --------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(n)
 * Average:   Theta(n^2)
 * Worst:     Theta(n^2)
 */

template <typename E, typename Comp>
class BubSort : public SortADT<E, Comp> {
public:
  // Bubble sort implementation
  void bubsort(E A[], int n) { // Bubble Sort
    for (int i=0; i<n-1; i++)     // Bubble up i'th record
      for (int j=n-1; j>i; j--)
        if (Comp::prior(A[j], A[j-1]))
          swap(A, j, j-1);
  }

  void sort(E* array, int n) {
    bubsort(array, n);
  }
};

#endif	/* b03_BUBSORT_H */

