// ALiu Edit: The function main of travtest.cpp is moved to DSTest.h and
//            renamed as function BinTreeTest
// Shaffer's Source code: travtest.cpp


#ifndef b11_BINTREE_H
#define	b11_BINTREE_H
#include "a01_book.h"
#include "b10_BinNodeADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.

// This program tests the relative efficiency for a number of
// preorder traversal implementations. 


// Binary tree node class implementation
template <typename E>
class BinNodePtr : public BinNode1<E> {
public:
  E it;                     // The node's value
  BinNodePtr* lc;              // Pointer to left child
  BinNodePtr* rc;              // Pointer to right child
public:
  // Two constructors -- with and without initial values
  BinNodePtr() { lc = rc = NULL; }
  BinNodePtr(E e, BinNodePtr* l =NULL,
                     BinNodePtr* r =NULL)
    { it = e; lc = l; rc = r; }
  ~BinNodePtr() {}             // Destructor
  E& val() { return it; }
  void setVal(const E& e) { it = e; }
  BinNode1<E>* left() const { return lc; }
  void setLeft(BinNode1<E>* b) { lc = (BinNodePtr*)b; }
  BinNode1<E>* right() const { return rc; }
  void setRight(BinNode1<E>* b) { rc = (BinNodePtr*)b; }
  bool isLeaf() { return (lc == NULL) && (rc == NULL); }
};


// Binary tree class
template <typename E>
class BinTree {
private:
  BinNode1<E>* root;   // Root of the BST
public:
  BinTree() { root = NULL; }  // Constructor
  BinNode1<E>* getroot() { return root; }
  void setroot(BinNode1<E>* newroot) { root = newroot; }
};


// Now we define several versions of preorder to test

// This implementation uses "check ahead" to call on children only
// if they are non-NULL. It uses access functions to get the children.
template <typename E>
void preorder1(BinNode1<E>* root)
{
  if (root->left() != NULL) preorder1(root->left());
  if (root->right() != NULL) preorder1(root->right());
}

// This implementation uses "check ahead" to call on children only
// if they are non-NULL. It looks directly at the child pointers.
template <typename E>
void preorder1a(BinNodePtr<E>* root)
{
  if (root->lc != NULL) preorder1a(root->lc);
  if (root->rc != NULL) preorder1a(root->rc);
}

// This implementation uses "check ahead" to call on children only
// if they are non-NULL. It avoids making 2 calls to the child
// access functions by calling it once and storing the result.
template <typename E>
void preorder1b(BinNodePtr<E>* root)
{
  BinNodePtr<E>* myl = root->lc;
  BinNodePtr<E>* myr = root->rc;
  if (myl != NULL) preorder1b(myl);
  if (myr != NULL) preorder1b(myr);
}

// This implementation uses "check ahead" to call on children only
// if they are non-NULL. However, it also does a check on the
// root first, to protect against an initial call on an empty tree.
// This means that all of the checks on children on essentially redundant.
// Thus, it measures the extra cost by checking each pointer twice.
template <typename E>
void preorder2(BinNode1<E>* root)
{
  if (root == NULL) return;  // Empty subtree, do nothing
  if (root->left() != NULL) preorder2(root->left());
  if (root->right() != NULL) preorder2(root->right());
}


// This implementation checks to see if the current node is NULL,
// but does not check the children. This results in recursive calls
// made on null children (effectively doubling the number of recursive
// calls). This version uses access functions to the children.
template <typename E>
void preorder3(BinNode1<E>* root)
{
  if (root == NULL) return;  // Empty subtree, do nothing
  preorder3(root->left());
  preorder3(root->right());
}

// This implementation checks to see if the current node is NULL,
// but does not check the children. This results in recursive calls
// made on null children (effectively doubling the number of recursive
// calls). This version directly accesses the child pointers.
template <typename E>
void preorder3a(BinNodePtr<E>* root)
{
  if (root == NULL) return;  // Empty subtree, do nothing
  preorder3a(root->lc);
  preorder3a(root->rc);
}


/// Insert a node into a random spot in a binary tree.
template <typename E>
BinNode1<E>* insert(BinNode1<E>* root, E val) {
  if (root == NULL)
    return new BinNodePtr<E>(val);
  else if (Random(2) == 0) {
    root->setLeft(insert(root->left(), val));
    return root;
  }
  else {
    root->setRight(insert(root->right(), val));
    return root;
  }
}


// Build a random binary tree with "numnodes" nodes
template <typename E>
void buildRandomTree(int numnodes, BinTree<E>* tree) {
  for(int i=0; i<numnodes; i++)
    tree->setroot(insert(tree->getroot(), i));
}

#endif	/* b11_BINTREE_H */

