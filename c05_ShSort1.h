// ALiu Edit: The template functions below are merged into template class
//            ShSort1.
//            The template function inssort2 is renamed as inssort1
//            The function main is removed.
// Shaffer's Source code: shsort.cpp


#ifndef b05_SHSORT1_H
#define	b05_SHSORT1_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Shell Sort ------------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(nlog(n))
 * Average:   N/A
 * Worst:     Theta(n^2)
 */

template <typename E, typename Comp>
class ShSort1 : public SortADT<E, Comp> {
public:
  // Shell sort implementation
  // Modified version of Insertion Sort for varying increments
  void inssort1(E A[], int n, int incr) {
    for (int i=incr; i<n; i+=incr)
      for (int j=i; (j>=incr) &&
                    (Comp::prior(A[j], A[j-incr])); j-=incr)
        swap(A, j, j-incr);
  }

  void shellsort1(E A[], int n) { // Shellsort
    for (int i=n/2; i>2; i/=2)      // For each increment
      for (int j=0; j<i; j++)       // Sort each sublist
        inssort1(&A[j], n-j, i);
    inssort1(A, n, 1);
  }

  void sort(E* array, int n) {
    shellsort1(array, n);
  }
};

#endif	/* b05_SHSORT1_H */

