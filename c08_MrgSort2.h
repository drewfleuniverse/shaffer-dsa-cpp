// ALiu Edit: The template functions below are merged into template class
//            MrgSort2.
//            The function main is removed.
// Shaffer's Source code: mrgsort2.cpp


#ifndef b08_MRGSORT2_H
#define	b08_MRGSORT2_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


template <typename E, typename Comp>
class MrgSort2 : public SortADT<E, Comp> {
public:
  // Mergesort implementation optimized to reverse the 2nd half,
  // so that there is no need to test for exhausted sublists
  void mergesort2(E A[], E temp[], int left, int right) {
    if (left == right) return;
    int i, j, k, mid = (left+right)/2;
    mergesort2(A, temp, left, mid);
    mergesort2(A, temp, mid+1, right);
    // Do the merge operation.  First, copy 2 halves to temp.
    for (i=left; i<=mid; i++) temp[i] = A[i];
    for (j=1; j<=right-mid; j++) temp[right-j+1] = A[j+mid];
    // Merge sublists back to A
    for (i=left,j=right,k=left; k<=right; k++)
      if (Comp::prior(temp[i], temp[j])) A[k] = temp[i++];
      else A[k] = temp[j--];
  }

  void sort(E* array, int n) {
    static E* temp = NULL;
    if (temp == NULL) temp = new E[n];  // Declare temp array
    mergesort2(array, temp, 0, n-1);
  }
};

#endif	/* b08_MRGSORT2_H */

