// ALiu Edit: The template functions below are merged into template class
//            HeapSort1.
//            The function main is removed.
// Shaffer's Source code: heapsort.cpp

#ifndef c14_HEAPSORT1_H
#define	c14_HEAPSORT1_H
#include "a01_book.h"
#include "b16_Heap1.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Heap Sort ------------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(nlog(n))
 * Average:   Theta(nlog(n))
 * Worst:     Theta(nlog(n))
 */

template <typename E, typename Comp>
class HeapSort1 : public SortADT<E, Comp> {
public:
  // Basic Heapsort version
  // Standard heapsort implementation
  void heapsort1(E A[], int n) { // Heapsort
    E maxval;
    Heap1<E,Comp> H(A, n, n);    // Build the heap
    for (int i=0; i<n; i++)        // Now sort
      maxval = H.removefirst();    // Place maxval at end
  }

  void sort(E* array, int n) {
  heapsort1(array, n);
  }
};

#endif	/* c14_HEAPSORT1_H */

