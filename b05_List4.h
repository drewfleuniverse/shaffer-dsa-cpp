// ALiu Edit: The class Link of dlinkFL.h is renamed as Link3
//            The class LList of dlist.h is renamed as LList3
// Shaffer's Source code: dlinkFL.h dlist.h


#ifndef b05_LIST4_H
#define	b05_LIST4_H
#include "a01_book.h"
#include "b01_ListADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Doubly Linked List, FL ------------------------------------------------------
 */

// Doubly linked list link node with freelist support
template <typename E> class Link3 {
private:
  static Link3<E>* freelist; // Reference to freelist head

public:
  E element;       // Value for this node
  Link3* next;         // Pointer to next node in list
  Link3* prev;         // Pointer to previous node

  // Constructors
  Link3(const E& it, Link3* prevp, Link3* nextp) {
    element = it;
    prev = prevp;
    next = nextp;
  }
  Link3(Link3* prevp =NULL, Link3* nextp =NULL) {
    prev = prevp;
    next = nextp;
  }

  void* operator new(size_t) {  // Overloaded new operator
    if (freelist == NULL) return ::new Link3; // Create space
    Link3<E>* temp = freelist; // Can take from freelist
    freelist = freelist->next;
    return temp;                 // Return the link
  }

  // Overloaded delete operator
  void operator delete(void* ptr) {
    ((Link3<E>*)ptr)->next = freelist; // Put on freelist
    freelist = (Link3<E>*)ptr;
  }
};

// The freelist head pointer is actually created here
template <typename E>
Link3<E>* Link3<E>::freelist = NULL;


// This is the declaration for DList. It is broken up because the
// methods that appear in the book are in a separate file.
// Linked list implementation
template <typename E> class LList3: public List<E> {
private:
  Link3<E>* head;       // Pointer to list header
  Link3<E>* tail;       // Pointer to list tailer
  Link3<E>* curr;       // Pointer ahead of current element
  int cnt;             // Size of list

  void init() {        // Intialization helper method
    curr = head = new Link3<E>;
    head->next = tail = new Link3<E>(head, NULL);
    cnt = 0;
  }

  void removeall() {   // Return link nodes to free store
    while(head != NULL) {
      curr = head;
      head = head->next;
      delete curr;
    }
  }

public:
  LList3(int size=defaultSize) { init(); } // Constructor
  ~LList3() { removeall(); }  // Destructor    // Destructor
  void clear() { removeall(); init(); }       // Clear list

  void moveToStart() // Place curr at list start
    { curr = head; }

  void moveToEnd()   // Place curr at list end
    { curr = tail->prev; }

  // Move fence one step right; no change if right is empty
  void next()
    { if (curr != tail->prev) curr = curr->next; }

  int length() const  { return cnt; }

  // Return the position of the current element
  int currPos() const {
    Link3<E>* temp = head;
    int i;
    for (i=0; curr != temp; i++)
      temp = temp->next;
    return i;
  }

  // Move down list to "pos" position
  void moveToPos(int pos) {
    Assert ((pos>=0)&&(pos<=cnt), "Position out of range");
    curr = head;
    for(int i=0; i<pos; i++) curr = curr->next;
  }

  const E& getValue() const { // Return current element
    if(curr->next == tail) return NULL;
    return curr->next->element;
  }

  // Include those  methods that are different from singly linked list
  // Insert "it" at current position
  void insert(const E& it) {
    curr->next = curr->next->prev =
      new Link3<E>(it, curr, curr->next);  
    cnt++;
  }

  // Append "it" to the end of the list.
  void append(const E& it) {
    tail->prev = tail->prev->next =
      new Link3<E>(it, tail->prev, tail);
    cnt++;
  }

  // Remove and return current element
  E remove() {
    if (curr->next == tail)        // Nothing to remove
      return NULL;
    E it = curr->next->element;    // Remember value
    Link3<E>* ltemp = curr->next;   // Remember link node
    curr->next->next->prev = curr;
    curr->next = curr->next->next; // Remove from list
    delete ltemp;                  // Reclaim space
    cnt--;                         // Decrement cnt
    return it;
  }

  // Move fence one step left; no change if left is empty
  void prev() {
    if (curr != head)  // Can't back up from list head
      curr = curr->prev;
  }
};

#endif	/* b05_LIST4_H */

