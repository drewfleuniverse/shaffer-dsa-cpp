// ALiu Edit: N/A
// Shaffer's Source code: None


#ifndef b01_SORTADT_H
#define	b01_SORTADT_H


template <typename E, typename Comp>
class SortADT {
private:
  void operator =(const SortADT&) {}
  SortADT(const SortADT&) {}
public:
  SortADT() {}
  virtual ~SortADT() {}
  virtual void sort(E* array, int n) = 0;
};

#endif	/* b01_SORTADT_H */

