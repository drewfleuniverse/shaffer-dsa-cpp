// ALiu Edit: The template functions below are merged into template class
//            InsSort.
//            The function main is removed.
// Shaffer's Source code: inssort.cpp


#ifndef b22_INSSORT_H
#define	b02_INSSORT_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Insertion Sort --------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(n)
 * Average:   Theta(n^2)
 * Worst:     Theta(n^2)
 */


template <typename E, typename Comp>
class InsSort : public SortADT<E, Comp> {
public:
  // Insertion sort implementation
  void inssort(E A[], int n) { // Insertion Sort
    for (int i=1; i<n; i++)       // Insert i'th record
      for (int j=i; (j>0) && (Comp::prior(A[j], A[j-1])); j--)
        swap(A, j, j-1);
  }

  void sort(E* array, int n) {
    inssort(array, n);
  }
};

#endif	/* b02_INSSORT_H */

