// ALiu Edit: The template functions below are merged into template class
//            SelSort.
//            The function main is removed.
// Shaffer's Source code: selsort.cpp


#ifndef b04_SELSORT_H
#define	b04_SELSORT_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Selection Sort --------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(n^2)
 * Average:   Theta(n^2)
 * Worst:     Theta(n^2)
 */

template <typename E, typename Comp>
class SelSort : public SortADT<E, Comp> {
public:
  // Selection sort implementation
  void selsort(E A[], int n) { // Selection Sort
    for (int i=0; i<n-1; i++) {   // Select i'th record
      int lowindex = i;           // Remember its index
      for (int j=n-1; j>i; j--)   // Find the least value
        if (Comp::prior(A[j], A[lowindex]))
    lowindex = j;           // Put it in place
      swap(A, i, lowindex);
    }
  }

  void sort(E* array, int n) {
    selsort(array, n);
  }
};

#endif	/* b04_SELSORT_H */

