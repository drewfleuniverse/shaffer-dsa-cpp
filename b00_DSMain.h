// ALiu Edit: The function main of Alistmain.cpp is renamed as testAList
//            The function main of SAlistmain.cpp is renamed as testSAList;
//              unused objects of SAlistmain.cpp are removed; some codes are
//              moved to a new function SAlistTest
//            The function main of Llistmain.cpp is renamed as testLList1
//            The function main of Dlistmain.cpp is renamed as testLList2
//            The function main of LlistFLmain.cpp is renamed as testLList3
//            The function main of Astackmain.cpp is renamed as testAStack
//            The function main of Lstackmain.cpp is renamed as testLStack
//            The function main of Aqueuemain.cpp is renamed as testAQueue
//            The function main of Lqueuemain.cpp is renamed as testLQueue
//            The function main of UALdictmain.cpp is renamed as testUALdict;
//              some codes are also removed
//            The function main of SALdictmain.cpp is renamed as testSALdict
// Shaffer's Source code: Alistmain.cpp    SAlistmain.cpp    Llistmain.cpp
//                        Dlistmain.cpp    LlistFLmain.cpp   Astackmain.cpp
//                        Lstackmain.cpp   Aqueuemain.cpp    Lqueuemain.cpp
//                        UALdictmain.cpp  SALdictmain.cpp

#ifndef b00_DSMAIN_H
#define	b00_DSMAIN_H
#include "a01_book.h"
#include "b00_DSTest.h"
#include "b02_List1.h"
#include "b03_List2.h"
#include "b04_List3.h"
#include "b05_List4.h"
#include "b07_Stack.h"
#include "b09_Queue.h"
#include "b14_Dict.h"


// Test Array-Based List
void testAList() {
// Declare some sample lists
  AList<Int*> L1;
  AList<Int*> L2(15);
  AList<Int> L3;

  // Call the generic list test functions with an array-based list
  ListTest<Int*, Int, AList<int> >(L1, L2, L3);
}

// Test Sorted Array-Based List
void testSAList() {
  // Declare some sample lists
  SAList<int,Int> L3;

  SAListTest(L3);
}

// Test Singly LInked List
void testLList1() {
  // Declare some sample lists
  LList1<Int*> L1;
  LList1<Int*> L2(15);
  LList1<Int> L3;

  ListTest<Int*, Int, LList1<int> >(L1, L2, L3);
}

// Test Singly LInked List with Freelist Support
void testLList2() {
  // Declare some sample lists
  LList2<Int*> L1;
  LList2<Int*> L2(15);
  LList2<Int> L3;

  ListTest<Int*, Int, LList2<int> >(L1, L2, L3);
}

// Test Doubly LInked List with Freelist Support
void testLList3() {
  // Declare some sample lists
  LList3<Int*> L1;
  LList3<Int*> L2(15);
  LList3<Int> L3;

  ListTest<Int*, Int, LList3<int> >(L1, L2, L3);
}

// Test Array-Based Stack
void testAStack() {
  // Declare some sample lists
  AStack<Int> S1;
  AStack<Int*> S2(15);
  AStack<int> S3;

  StackTest<Int>(S1);
  StackTest<int>(S3);
}

// Test Linked Stack
void testLStack() {
  // Declare some sample lists
  LStack<Int> S1;
  LStack<Int*> S2(15);
  LStack<int> S3;

  StackTest<Int>(S1);
  StackTest<int>(S3);
}

// Test Array-Based Queue
void testAQueue() {
  // Declare some sample lists
  AQueue<Int> S1;
  AQueue<Int*> S2(15);
  AQueue<int> S3;

  QueueTest<Int>(S1);
  QueueTest<int>(S3);
}

// Test Linked Queue
void testLQueue() {
  // Declare some sample lists
  LQueue<Int> S1;
  LQueue<Int*> S2(15);
  LQueue<int> S3;

  QueueTest<Int>(S1);
  QueueTest<int>(S3);
}

// Test Dictionary with Unsorted Array-Based List
void testUALdict() {
  UALdict<int, Int*> dict;

  DictTest(dict);
}

// Test Dictionary with Sorted Array-Based List
void testSALdict() {
  SALdict<int, Int*> dict;

  DictTest(dict);
}

// Test Binary Tree
void testBinTree() {
  BinTreeTest(1500, 1500);
}

// Test Binary Tree Node Variant 1
void testVarBinNode1() {
  VarBinNode1Test();
}
// Test Binary Tree Node Variant 2
void testVarBinNode2() {
  VarBinNode2Test();
}

// Test Binary Search Tree
void testBST() {
  BSTTest();
}

// Test Heap
void testHeap1() {
  HeapTest(10);
}

// Test Hash Table
void testHash() {
  HashTest();
}

#endif	/* b00_DSMAIN_H */

