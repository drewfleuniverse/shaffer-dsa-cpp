// ALiu Edit: N/A
// Shaffer's Source code: None


#include <iostream>
#include <string>
#include <vector>
#include "a00_CompMain.h"
#include "b00_DSMain.h"
#include "c00_ALMain.h"
#include "d00_SearchMain.h"
using namespace std;


int main(int argc, char** argv) {
    string dash = "--------------------------------------"
            "------------------------------------------\n";

    // Comparators -------------------------------------------------------------
    cout << "Begin testing Comparators:\n" << dash;
    CompTest(1000000);
    
    // Lists -------------------------------------------------------------------
    cout << "Begin testing AList:\n" << dash;
    testAList();
    cout << "Begin testing SAList:\n" << dash;
    testSAList();
    cout << "Begin testing LList, Singly:\n" << dash;
    testLList1();
    cout << "Begin testing LList, Singly with Freelist Support:\n" << dash;
    testLList2();
    cout << "Begin testing LList, Doubly with Freelist Support:\n" << dash;
    testLList3();

    // Stacks ------------------------------------------------------------------
    cout << "Begin testing Array-Based Stack:\n" << dash;
    testAStack();
    cout << "Begin testing Linked Stack:\n" << dash;
    testLStack();

    // Queues ------------------------------------------------------------------
    cout << "Begin testing Array-Based Queue:\n" << dash;
    testAQueue();
    cout << "Begin testing Linked Queue:\n" << dash;
    testLQueue();

    // Dictionaries ------------------------------------------------------------
    cout << "Begin testing Dictionary with Unsorted Array-Based List:\n" << dash;
    testUALdict();
    cout << "Begin testing Dictionary with Sorted Array-Based List:\n" << dash;
    testSALdict();

    // Binary Trees ------------------------------------------------------------
    cout << "Begin testing Binary Tree:\n" << dash;
    testBinTree();
    cout << "Begin testing Binary Tree Node Variant 1:\n" << dash;
    testVarBinNode1();
    cout << "Begin testing Binary Tree Node Variant 2:\n" << dash;
    testVarBinNode2();
    cout << "Begin testing Binary Search Tree:\n" << dash;
    testBST();

    // Heaps -------------------------------------------------------------------
    cout << "Begin testing Binary Tree:\n" << dash;
    testHeap1();
    
    // Hash Table --------------------------------------------------------------
    cout << "Begin testing Hash Table:\n" << dash;
    testHash();
    
    // Sort --------------------------------------------------------------------
    cout << "Begin testing Insertion Sort:\n" << dash;
    testInssort();
    cout << "Begin testing Bubble Sort:\n" << dash;
    testBubSort();
    cout << "Begin testing Selection Sort:\n" << dash;
    testSelSort();
    cout << "Begin testing Shell Sort using Insertion Sort:\n" << dash;
    testShSort1();
    cout << "Begin testing Shell Sort using Insertion Sort, Optimized:\n"<<dash;
    testShSort2();
    cout << "Begin testing Merge Sort:\n" << dash;
    testMrgSort1();
    cout << "Begin testing Merge Sort, Optimized:\n" << dash;
    testMrgSort2();
    cout<<"Begin testing Merge Sort, Optimized with InsSort Threshold:\n"<<dash;
    testMrgSort3();
    cout << "Begin testing Quick Sort:\n" << dash;
    testQSort1();
    cout<<"Begin testing Quick Sort, Optimized with InsSort Threshold:\n"<<dash;
    testQSort2();
    cout << "Begin testing Quick Sort, Optimized, "
            "No Function Calls and No Recursion:\n" << dash;
    testQSort3();
    cout << "Begin testing Quick Sort, Optimized with InsSort Threshold, "
            "No Function Calls and No Recursion:\n" << dash;
    testQSort4();
    cout << "Begin testing Heap Sort:\n" << dash;
    testHeapSort1();
    cout << "Begin testing Heap Sort with Modified Siftdown:\n" << dash;
    testHeapSort2();
    cout << "Begin testing Heap Sort, "
            "Optimized with Fewer Function Calls:\n" << dash;
    testHeapSort3();
    
    // Search ------------------------------------------------------------------
    cout << "Begin testing Binary Search:\n" << dash;
    testBSearch();
    return 0;
}

