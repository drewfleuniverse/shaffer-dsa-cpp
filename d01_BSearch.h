// ALiu Edit: Note that the function main is moved to SearchTest.h
// Shaffer's Source code: bsearch.cpp


#ifndef d01_BSEARCH_H
#define	d01_BSEARCH_H
#include "a01_book.h"
#include "a02_compare.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Binary Search ------------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(1)
 * Average:   Theta(log(n))
 * Worst:     Theta(log(n))
 */

// Perform binary search on an array. There are two versions:
// One that uses a template and a comparator, and one that works
// only for a simple int array.

// This version is generalized to use a template for elements,
// and a comparator.
// Return the position of an element in a sorted array of
// size n with key value K.  If none exist, return n.
template<typename Key, typename E, typename Comp, typename getKey>
int binaryt(E array[], int n, Key K) {
  int l = -1;
  int r = n;     // l and r are beyond the bounds of array
  while (l+1 != r) {   // Stop when l and r meet
    int i = (l+r)/2;   // Check middle of remaining subarray
    if (Comp::lt(K, getKey::key(array[i]))) r = i;    // In left half
    if (Comp::eq(K, getKey::key(array[i]))) return i; // Found it
    if (Comp::gt(K, getKey::key(array[i]))) l = i;    // In right half
  }
  return n; // Search value not in array
}

// This is the version that appears in the book.
// It works on an integer array.
// Return the position of an element in sorted array "A" of
// size "n" with value "K".  If "K" is not in "A", return
// the value "n".
int binary(int A[], int n, int K) {
  int l = -1;
  int r = n;          // l and r are beyond array bounds
  while (l+1 != r) {  // Stop when l and r meet
    int i = (l+r)/2;  // Check middle of remaining subarray
    if (K < A[i]) r = i;     // In left half
    if (K == A[i]) return i; // Found it
    if (K > A[i]) l = i;     // In right half
  }
  return n; // Search value not in A
}

#endif	/* d01_BSEARCH_H */

