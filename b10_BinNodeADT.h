// ALiu Edit: The BinNode of bnadt.h is renamed as BinNode1.
//            The BinNode of BinNode.h is renamed as BinNode2.
// Shaffer's Source code: bnadt.h BinNode.h


#ifndef b10_BINNODEADT_H
#define	b10_BINNODEADT_H
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Binary Tree Node ADT --------------------------------------------------------
 */

// Binary tree node abstract class
template <typename E> class BinNode1 {
public:
  virtual ~BinNode1() {} // Base destructor

  // Return the node's value
  virtual E& val() = 0;

  // Set the node's value
  virtual void setVal(const E&) = 0;

  // Return the node's left child
  virtual BinNode1* left() const = 0;

  // Set the node's left child
  virtual void setLeft(BinNode1*) = 0;

  // Return the node's right child
  virtual BinNode1* right() const = 0;

  // Set the node's right child
  virtual void setRight(BinNode1*) = 0;

  // Return true if the node is a leaf, false otherwise
  virtual bool isLeaf() = 0;
};

// Binary tree node abstract class
template <typename E> class BinNode2 {
public:
  virtual ~BinNode2() {} // Base destructor

  // Return the node's value
  virtual E& element() = 0;

  // Set the node's value
  virtual void setElement(const E&) = 0;

  // Return the node's left child
  virtual BinNode2* left() const = 0;

  // Set the node's left child
  virtual void setLeft(BinNode2*) = 0;

  // Return the node's right child
  virtual BinNode2* right() const = 0;

  // Set the node's right child
  virtual void setRight(BinNode2*) = 0;

  // Return true if the node is a leaf, false otherwise
  virtual bool isLeaf() = 0;
};

#endif	/* b10_BINNODEADT_H */

