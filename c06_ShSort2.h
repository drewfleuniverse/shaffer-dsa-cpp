// ALiu Edit: The template functions below are merged into template class
//            ShSort2.
//            The function main is removed.
// Shaffer's Source code: shsort2.cpp


#ifndef b06_SHSORT2_H
#define	b06_SHSORT2_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Shell Sort ------------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(nlog(n))
 * Average:   Omicron(n^1.5), optimized with division by three increments
 * Worst:     Theta(n^2)
 */

template <typename E, typename Comp>
class ShSort2 : public SortADT<E, Comp> {
public:
  // Optimized Shell sort implementation, uses "division by 3's"
  // to determine growth in list sizes
  // Modified version of Insertion Sort for varying increments
  void inssort2(E A[], int n, int incr) {
    for (int i=incr; i<n; i+=incr)
      for (int j=i; (j>=incr) && (Comp::prior(A[j], A[j-incr])); j-=incr)
        swap(A, j, j-incr);
  }

  void shellsort2(E A[], int n) {
    int firstincr;
    for (firstincr = 1; firstincr<=n; firstincr = firstincr*3 + 1);
    for (int i=firstincr/3; i>2; i/=3)
      for (int j=0; j<i; j++)
        inssort2(&A[j], n-j, i);
    inssort2(A, n, 1);    // Could call regular inssort
  }

  void sort(E* array, int n) {
    shellsort2(array, n);
  }
};

#endif	/* b06_SHSORT2_H */

