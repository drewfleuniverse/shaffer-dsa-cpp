// ALiu Edit: The template functions below are merged into template class
//            HeapSort3.
//            The function main is removed.
// Shaffer's Source code: heapsort3.cpp


#ifndef c16_HEAPSORT3_H
#define	c16_HEAPSORT3_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


template <typename E, typename Comp>
class HeapSort3 : public SortADT<E, Comp> {
public:
  // Optimized version of heapsort. All node access functions are
  // replaced with direct computations on the array indices, so that there
  // are far fewer function calls.
  void heapsort3(E A[], int n)
  { // Heapsort 
    int pos;
    for (int i = n / 2 - 1; i >= 0; i--) {
      pos = i;
      while (pos < n / 2) { // Stop if pos is a leaf 
        int j = 2 * pos + 1;
        if (((j + 1) < n) && (A[j] < A[j + 1]))
    j++; // Set j to greater child's value 
        if (!(A[pos] < A[j]))
    break; // Done 
        {
    E temp = A[pos];
    A[pos] = A[j];
    A[j] = temp;
        } // swap 
        pos = j; // Move down 
      }
    }
    for (n--; n > 0; n--) { // Now sort 
      // Swap max with last value 
      {
        E temp = A[0];
        A[0] = A[n];
        A[n] = temp;
      } // swap 
      pos = 0;
      while (pos < n / 2) { // Stop if pos is a leaf 
        int j = 2 * pos + 1;
        if (((j + 1) < n) && (A[j] < A[j + 1]))
    j++; // Set j to greater child's value 
        if (!(A[pos] < A[j]))
    break; // Done 
        // swap 
        {
    E temp = A[pos];
    A[pos] = A[j];
    A[j] = temp;
        }
        pos = j; // Move down 
      }
    }
  }

  void sort(E* array, int n) {
   heapsort3(array, n);
  }
};

#endif	/* c16_HEAPSORT3_H */

