// ALiu Edit: All class names from varnodei.cpp are appended with 1
//            All class names from p.165 (Shaffer, 2013) are appended with 2
// Shaffer's Source code: varnodei.cpp
// p.165 In: Shaffer, C. A. (2013) Data Structure and Algorithm Analysis - C++.


#ifndef b12_VARBINNODE_H
#define	b12_VARBINNODE_H
#include "a01_book.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.

/* Binary Tree Node Variants ---------------------------------------------------
 */

typedef char Operator;
typedef std::string Operand;

// Node implementation with simple inheritance
class VarBinNode1 {   // Node abstract base class
public:
  virtual ~VarBinNode1() {}
  virtual bool isLeaf() = 0;    // Subclasses must implement
};

class LeafNode1 : public VarBinNode1 { // Leaf node
private:
  Operand var;                       // Operand value

public:
  LeafNode1(const Operand& val) { var = val; } // Constructor
  bool isLeaf() { return true; }     // Version for LeafNode
  Operand value() { return var; }    // Return node value
};

class IntlNode1 : public VarBinNode1 { // Internal node
private:
  VarBinNode1* left;                  // Left child
  VarBinNode1* right;                 // Right child
  Operator opx;                      // Operator value

public:
  IntlNode1(const Operator& op, VarBinNode1* l, VarBinNode1* r)
    { opx = op; left = l; right = r; } // Constructor
  bool isLeaf() { return false; }    // Version for IntlNode
  VarBinNode1* leftchild() { return left; }   // Left child
  VarBinNode1* rightchild() { return right; } // Right child
  Operator value() { return opx; }           // Value
};

void traverse1(VarBinNode1 *root) {    // Preorder traversal
  if (root == NULL) return;          // Nothing to visit
  if (root->isLeaf())                // Do leaf node
    std::cout << "Leaf: " << ((LeafNode1 *)root)->value() << std::endl;
  else {                             // Do internal node
    std::cout << "Internal: "
         << ((IntlNode1 *)root)->value() << std::endl;
    traverse1(((IntlNode1 *)root)->leftchild());
    traverse1(((IntlNode1 *)root)->rightchild());
  }
}


// Node implementation with the composite design pattern
class VarBinNode2 { // Node abstract base class
public:
  virtual ~VarBinNode2() {} // Generic destructor
  virtual bool isLeaf() = 0;
  virtual void traverse() = 0;
};

class LeafNode2 : public VarBinNode2 { // Leaf node
private:
  Operand var; // Operand value
  
public:
  LeafNode2(const Operand& val) { var = val; } // Constructor
  bool isLeaf() { return true; } // isLeaf for Leafnode
  Operand value() { return var; } // Return node value
  void traverse() { std::cout << "Leaf: " << value() << std::endl; }
};

class IntlNode2 : public VarBinNode2 { // Internal node
private:
  VarBinNode2* lc; // Left child
  VarBinNode2* rc; // Right child
  Operator opx; // Operator value
  
public:
  IntlNode2(const Operator& op, VarBinNode2* l, VarBinNode2* r)
  { opx = op; lc = l; rc = r; } // Constructor
  bool isLeaf() { return false; } // isLeaf for IntlNode
  VarBinNode2* left() { return lc; } // Left child
  VarBinNode2* right() { return rc; } // Right child
  Operator value() { return opx; } // Value
  void traverse() { // Traversal behavior for internal nodes
    std::cout << "Internal: " << value() << std::endl;
    if (left() != NULL) left()->traverse();
    if (right() != NULL) right()->traverse();
  }
};

  // Do a preorder traversal
void traverse2(VarBinNode2 *root) {
  if (root != NULL) root->traverse();
}

#endif	/* b12_VARBINNODE_H */

