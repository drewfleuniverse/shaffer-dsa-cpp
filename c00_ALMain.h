// ALiu Edit: This file wraps testing procedures for sort algorithms
// Shaffer's Source code: None


#ifndef b00_ALMAIN_H
#define	b00_ALMAIN_H
#include "a02_compare.h"
#include "c00_ALTest.h"
#include "c02_InsSort.h"
#include "c03_BubSort.h"
#include "c04_SelSort.h"
#include "c05_ShSort1.h"
#include "c06_ShSort2.h"
#include "c07_MrgSort1.h"
#include "c08_MrgSort2.h"
#include "c09_MrgSort3.h"
#include "c10_QSort1.h"
#include "c11_QSort2.h"
#include "c12_QSort3.h"
#include "c13_QSort4.h"
#include "c14_HeapSort1.h"
#include "c15_HeapSort2.h"
#include "c16_HeapSort3.h"
const int arraysize = 40000;
const int listsize = 40000;

// Test Insertion Sort
void testInssort() {
  InsSort<int, minintCompare> IS;
  SortTest<int, minintCompare>(IS, arraysize, listsize);
}

// Test Bubble Sort
void testBubSort() {
  BubSort<int, minintCompare> BS;
  SortTest<int, minintCompare>(BS, arraysize, listsize);
}

// Test Selection Sort
void testSelSort() {
  SelSort<int, minintCompare> SS;
  SortTest<int, minintCompare>(SS, arraysize, listsize);
}

// Test Shell Sort using Insertion Sort
void testShSort1() {
  ShSort1<int, minintCompare> SS1;
  SortTest<int, minintCompare>(SS1, arraysize, listsize);
}

// Test Shell Sort using Insertion Sort, Optimized
void testShSort2() {
  ShSort2<int, minintCompare> SS2;
  SortTest<int, minintCompare>(SS2, arraysize, listsize);
}

// Test Merge Sort
void testMrgSort1() {
  MrgSort1<int, minintCompare> MS1;
  SortTest<int, minintCompare>(MS1, arraysize, listsize);
}

// Test Merge Sort, Optimized
void testMrgSort2() {
  MrgSort2<int, minintCompare> MS2;
  SortTest<int, minintCompare>(MS2, arraysize, listsize);
}

// Test Merge Sort, Optimized with InsSort Threshold
void testMrgSort3() {
  MrgSort3<int, minintCompare> MS3;
  SortTest<int, minintCompare>(MS3, arraysize, listsize);
}

// Test Quick Sort
void testQSort1() {
  QSort1<int, minintCompare> QS1;
  SortTest<int, minintCompare>(QS1, arraysize, listsize);
}

// Test Quick Sort, Optimized with InsSort Threshold
void testQSort2() {
  QSort2<int, minintCompare> QS2;
  SortTest<int, minintCompare>(QS2, arraysize, listsize);
}

// Test Quick Sort, Optimized, No Function Calls and No Recursion
void testQSort3() {
  QSort3<int, minintCompare> QS3;
  SortTest<int, minintCompare>(QS3, arraysize, listsize);
}

// Test Quick Sort, Optimized with InsSort Threshold,
// No Function Calls and No Recursion
void testQSort4() {
  QSort4<int, minintCompare> QS4;
  SortTest<int, minintCompare>(QS4, arraysize, listsize);
}

// Test Heap Sort
void testHeapSort1() {
  HeapSort1<int, maxintCompare> HS1;
  SortTest<int, maxintCompare>(HS1, arraysize, listsize);
}

// Test Heap Sort with Modified Siftdown
void testHeapSort2() {
  HeapSort2<int, maxintCompare> HS2;
  SortTest<int, maxintCompare>(HS2, arraysize, listsize);
}

// Test Heap Sort, Optimized with Fewer Function Calls
void testHeapSort3() {
  HeapSort3<int, maxintCompare> HS3;
  SortTest<int, maxintCompare>(HS3, arraysize, listsize);
}

#endif	/* b00_ALMAIN_H */

