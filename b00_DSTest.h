// ALiu Edit: The function dicttest of DictTest.h is renamed as DictTest
//            The function BinTreeTest is modified from the function main of 
//              travtest.cpp
//            The function VarBinNode1Test and VarBinNode2Test are copied from
//              the function main of varnodei.cpp
//            The function BSTTest is copied from the function main of 
//              BSTmain.cpp
//            The function HeapTest is modified from the function main of 
//              heapmain.cpp. And the function permute is moved to book.h
// Shaffer's Source code: ListTest.h    StackTest.h    QueueTest.h
//                        travtest.cpp  varnodei.cpp   BSTmain.cpp
//                        heapmain.cpp  SAlistmain.cpp DictTest.h


#ifndef b00_DSTEST_H
#define	b00_DSTEST_H
#include "a01_book.h"
#include "a02_compare.h"
#include "b01_ListADT.h"
#include "b06_StackADT.h"
#include "b08_QueueADT.h"
#include "b13_DictADT.h"
#include "b11_BinTree.h"
#include "b12_VarBinNode.h"
#include "b15_BST.h"
#include "b16_Heap1.h"
#include "b17_Heap2.h"
#include "b18_HashDict.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* List Test -------------------------------------------------------------------
 */

// Full template version of find function
template <typename Key, typename E, typename Compare, typename getKey>
bool findt(List<E>& L, Key K, E& it) {
  for (L.moveToStart(); L.currPos()<L.length(); L.next()) {
    it = L.getValue();
    if (Compare::eq(K, getKey::key(it))) return true;
  }
  return false;  // Value not found
}

// Specialization of the find function for ints, as used in the book.
// This int-based version is used at that point in the book so that
// the issues of comparators and keys can be deferred.
// Return true if "K" is in list "L", false otherwise
bool find(List<int>& L, int K) {
  int it;
  for (L.moveToStart(); L.currPos()<L.length(); L.next()) {
    it = L.getValue();
    if (K == it) return true;  // Found K
  }
  return false;                // K not found
}

// Print out the list (including showing position for the fence)
// Print list contents
template <typename E>
void lprint(List<E>& L) {
  // We don't want to screw up the current position of the list
  int currpos = L.currPos();

  L.moveToStart();

  cout << "< ";
  int i;
  for (i=0; i<currpos; i++) {
    cout << L.getValue() << " ";
    L.next();
  }
  cout << "| ";
  while (L.currPos()<L.length()) {
    cout << L.getValue() << " ";
    L.next();
  }
  cout << ">\n";
  L.moveToPos(currpos); // Reset the fence to its original position
}

// Here is test code to exercise the various methods
template <typename E1, typename E2, typename ListImp>
void ListTest(List<E1>& L1, List<E1>& L2, List<E2>& L3) {
  E1 temp1;
  E1 temp2;
  E2 temp3;

  lprint(L2);
  cout << "Size: " << L2.length() << endl;
  L2.clear();

  L2.insert(new Int(1));
  lprint(L2);
  cout << "Size: " << L2.length() << endl;
  L2.clear();

  L2.append(new Int(1));
  lprint(L2);
  cout << "Size: " << L2.length() << endl;

  L2.clear();
  L2.append(new Int(1));
  L2.moveToEnd();
  lprint(L2);
  L2.clear();

  // Test a bunch of list operations
  L2.append(new Int(1));
  temp2 = L2.remove();
  lprint(L2);
  L2.append(new Int(10));
  lprint(L2);
  L2.append(new Int(20));
  L2.append(new Int(15));
  lprint(L2);
  L1.moveToStart();
  L1.insert(new Int(39));
  L1.next();
  L1.insert(new Int(9));
  L1.insert(new Int(5));
  L1.append(new Int(4));
  L1.append(new Int(3));
  L1.append(new Int(2));
  L1.append(new Int(1));
  lprint(L1);
  L1.moveToStart();
  if (! findt<int, E1, intintCompare, getIntsKey>(L1, 3, temp1))
    cout << "Value 3 not found.\n";
  else cout << "Found " << temp1 << endl;
  lprint(L1);
  if (! findt<int, E1, intintCompare, getIntsKey>(L1, 3, temp1))
    cout << "Value 3 not found.\n";
  else cout << "Found " << temp1 << endl;
  lprint(L1);
  L1.moveToStart();
  if (! findt<int, E1, intintCompare, getIntsKey>(L1, 29, temp1))
    cout << "Value 29 not found.\n";
  else cout << "Found " << temp1 << endl;
  if (! findt<int, E1, intintCompare, getIntsKey>(L1, 5, temp1))
    cout << "Value 5 not found.\n";
  else cout << "Found " << temp1 << endl;
  L1.moveToStart();
  if (! findt<int, E1, intintCompare, getIntsKey>(L1, 5, temp1))
    cout << "Value 5 not found.\n";
  else cout << "Found " << temp1 << endl;

  L2.moveToStart();
  if (L2.currPos()<L2.length()) {
    temp2 = L2.getValue();
    cout << "L2 curr: " << temp2 << endl;
  }
  else
    cout << "L2: Nothing found\n";
  cout << "L1: ";  lprint(L1);
  cout << "Size: " << L1.length() << endl;
  cout << "L2: ";  lprint(L2);
  cout << "L3: ";  lprint(L3);
  L3.insert(3);
  cout << "L3: ";  lprint(L3);
  L2.moveToStart();
  L2.next();
  L1.moveToStart();
  if (L1.currPos()<L1.length()) {
    temp1 = L1.getValue();
    cout << "L1 curr: " << temp1 << endl;
  }
  else
    cout << "L1: Nothing found\n";
  if (L1.currPos()<L1.length()) {
    temp1 = L1.remove();
    cout << "Deleted: " << temp1 << endl;
  }
  else
    cout << "L1: Nothing found\n";

  L1.insert(new Int(42));
  lprint(L1);
  L1.moveToStart();
  if (! findt<int, E1, intintCompare, getIntsKey>(L1, 4, temp1))
    cout << "Value 4 not found.\n";
  else cout << "Found " << temp1 << endl;
  L2.moveToStart();

  if (L2.currPos()<L2.length()) {
    temp2 = L2.remove();
    cout << "Deleted: " << temp2 << endl;
  }
  else
    cout << "L2: Nothing found\n";

  if (L2.currPos()<L2.length()) {
    temp2 = L2.remove();
    cout << "Deleted: " << temp2 << endl;
  }
  else
    cout << "L2: Nothing found\n";

  lprint(L2);
  cout << "Size: " << L2.length() << endl;
  L2.clear();  lprint(L2);
  cout << "Size: " << L2.length() << endl;
  L2.append(new Int(5));  lprint(L2);
  L2.moveToEnd();

  cout << "Print it again\n";
  lprint(L2);

  if (L2.currPos()<L2.length()) {
    temp1 = L2.getValue();
    cout << "L2 curr: " << temp2 << endl;
  }
  else
    cout << "L2: Nothing found\n";

  if (L2.currPos()<L2.length()) {
    temp2 = L2.remove();
    cout << "Deleted: " << temp2 << endl;
  }
  else
    cout << "L2: Nothing to remove!\n";

  cout << "Start a new round\n";
  L2.clear();
  cout << "cleared\n";
  lprint(L2);
  L2.moveToStart();
  L2.insert(new Int(1));
  lprint(L2);
  L2.insert(new Int(2));
  lprint(L2);
  L2.moveToPos(2);
  L2.insert(new Int(3));
  cout << "L2: "; lprint(L2);
  cout << "L1: "; lprint(L1);
  temp1 = L1.remove();
  cout << "L1: "; lprint(L1);
  L2.clear();

  cout << "Now, test int find\n";
  ListImp MyList;
  MyList.insert(3);
  if (! find(MyList, 3))
    cout << "Value 3 not found.\n";
  else cout << "Found 3\n";
  if (! find(MyList, 13))
    cout << "Value 13 not found.\n";
  else cout << "Found 13\n";

  cout << "That is all.\n\n";
}


/* Sorted Array-Based List Test -------------------------------------------------------------------
 */

template <typename E> void SAListTest(SAList<E, Int>& L) {
  Int dum;
  KVpair<E,Int>* T2;
  T2 = new KVpair<E,Int>(5, 5);
  L.insert(*T2);
  delete(T2);
  T2 = new KVpair<E,Int>(3, 3);
  L.insert(*T2);
  delete(T2);
  T2 = new KVpair<E,Int>(7, 7);
  L.insert(*T2);
  delete(T2);
  //  L3.print();
  L.moveToStart();
  KVpair<E,Int> temp2;
  while (L.currPos() < L.length()) {
    temp2 = L.remove();
    cout << "Removed " << temp2.key() << endl;
  }
}


/* Stack Test ------------------------------------------------------------------
 */

// Calls for testing stack implementations
template <typename E> void StackTest(Stack<E>& St) {
  E temp;

  // Test a bunch of stack operations
  if (St.length() > 0) {
    temp = St.pop();
    cout << "Top is " << temp << endl;
  }
  else cout << "Nothing in stack\n";
  St.push(10);
  St.push(20);
  St.push(15);
  cout << "Size is " << St.length() << endl;
  if (St.length() > 0) {
    temp = St.pop();
    cout << "Top is " << temp << endl;
  }
  else cout << "Nothing in stack\n";
  while(St.length() > 0) {
    temp = St.pop();
    cout << temp << " ";
  }
  cout << endl;
  if (St.length() > 0) {
    temp = St.pop();
    cout << "Top is " << temp << endl;
  }
  else cout << "Nothing in stack\n";
  cout << "Size is " << St.length() << endl;
  St.clear();
  cout << "Size is " << St.length() << endl;
  cout << "That is all.\n\n";
}


/* Queue Test ------------------------------------------------------------------
 */

template <typename E> void QueueTest(Queue<E>& Que) {
  E temp;

  // Test a bunch of queue operations
  Que.enqueue(10);
  Que.enqueue(20);
  Que.enqueue(15);
  cout << "Length is " << Que.length() << endl;
  if (Que.length() > 0) {
    temp = Que.frontValue();
    cout << "Front is " << temp << endl;
  }
  else cout << "Nothing in queue\n";
  while(Que.length() > 0) {
    temp = Que.dequeue();
    cout << temp << " ";
  }
  cout << endl;
  cout << "Length is " << Que.length() << endl;
  if (Que.length() > 0) {
    temp = Que.frontValue();
    cout << "Front is " << temp << endl;
  }
  else cout << "Nothing in queue\n";
  Que.clear();
  cout << "Length is " << Que.length() << endl;
  cout << "That is all.\n\n";
}


/* Dictionary Test ------------------------------
 */

void DictTest(Dictionary<int, Int*> &dict) {
  Int* val;

  dict.insert(10, new Int(10));
  if ((val = dict.find(10)) != NULL)
    cout << "Found value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to match key value 10\n";
  dict.insert(15, new Int(15));
  if ((val = dict.find(5)) != NULL)
    cout << "Found value " << val << " to match key value 5\n";
  else
    cout << "Nothing found to match key value 5\n";
  dict.insert(10, new Int(10));
  cout << "Size is " << dict.size() << endl;
  if ((val = dict.find(10)) != NULL)
    cout << "Found value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to match key value 10\n";
  if ((val = dict.remove(10)) != NULL)
    cout << "Removed value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to match key value 10\n";
  if ((val = dict.find(10)) != NULL)
    cout << "Found value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to match key value 10\n";
  if ((val = dict.remove(10)) != NULL)
	cout << "Remove value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to remove with key value 10\n";
  if ((val = dict.remove(10)) != NULL)
	cout << "Remove value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to remove with key value 10\n";
  cout << "Size is " << dict.size() << endl;
  dict.clear();
  if ((val = dict.find(10)) != NULL)
    cout << "Found value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to match key value 10\n";
  cout << "Size is " << dict.size() << endl;

  cout << "Now, do interator\n";
  dict.insert(25, new Int(25));
  dict.insert(30, new Int(30));
  dict.insert(21, new Int(21));
  Int* e;
  while (dict.size() > 0) {
    e = dict.removeAny();
    cout << "Got element " << e << '\n';
  }
  cout << endl;
}


/* Binary Tree Test ------------------------------------------------------------
 */

void BinTreeTest(int nodes, int runs) {
  BinTree<int>* tree = new BinTree<int>();

  /*Assert(argc == 3, "Usage: travtest <numnodes> <numruns>");*/

  int numnodes = nodes/*atoi(argv[1])*/;
  int numruns = runs/*atoi(argv[2])*/;
  int i;
  Randomize();
  cout << "Build a tree with " << numnodes << " nodes\n";
  buildRandomTree(numnodes, tree);

  Settime();
  for (i=0; i<numruns; i++)
    preorder1(tree->getroot());
  cout << "Time for check-ahead (preorder1): "
	   << Gettime() << " sec\n";

  Settime();
  for (i=0; i<numruns; i++)
    preorder1a((BinNodePtr<int>*)tree->getroot());
  cout << "Time for stripped check-ahead (preorder1a): "
       << Gettime() << " sec\n";

  Settime();
  for (i=0; i<numruns; i++)
    preorder1b((BinNodePtr<int>*)tree->getroot());
  cout << "Time for prestored check-ahead (preorder1b): "
       << Gettime() << " sec\n";

  Settime();
  for (i=0; i<numruns; i++)
    preorder2(tree->getroot());
  cout << "Time for check-ahead with extra check (preorder2): "
       << Gettime() << " sec\n";

  Settime();
  for (i=0; i< numruns; i++)
    preorder3(tree->getroot());
  cout << "Time for pre-check with extra recursive calls (preorder3): "
       << Gettime() << " sec\n";

  Settime();
  for (i=0; i< numruns; i++)
    preorder3a((BinNodePtr<int>*)tree->getroot());
  cout << "Time for stripped pre-check with extra recursive calls (preorder3a): "
       << Gettime() << " sec\n\n";
}

void VarBinNode1Test() {
  VarBinNode1* temp1;
  VarBinNode1* temp2;
  VarBinNode1* root;
  std::string string1 = "Hello1";
  std::string string2 = "Another string";

  temp1 = new LeafNode1(string1);
  temp2 = new LeafNode1(string2);
  root = new IntlNode1('+', temp1, temp2);
  traverse1(root);
  cout << '\n';
}

void VarBinNode2Test() {
  VarBinNode2* temp1;
  VarBinNode2* temp2;
  VarBinNode2* root;
  std::string string1 = "Hello1";
  std::string string2 = "Another string";

  temp1 = new LeafNode2(string1);
  temp2 = new LeafNode2(string2);
  root = new IntlNode2('+', temp1, temp2);
  traverse2(root);
  cout << '\n';
}

void BSTTest() {
  BST<int, Int*> tree;
  Int* it;

  cout << "Size: " << tree.size() << "\n";
  tree.insert(100, new Int(100));
  tree.print();
  cout << "Size: " << tree.size() << "\n";
  it = tree.remove(10);
  tree.print();
  cout << "Size: " << tree.size() << "\n";
  tree.clear();
  cout << "Cleared.\n";
  cout << "Size: " << tree.size() << "\n";
  tree.insert(15, new Int(15));
  cout << "Size: " << tree.size() << "\n";
  if ((it = tree.find(20)) != NULL)
    cout << "Found " << it << endl;
  else cout << "No key 20\n";
  if ((it = tree.find(15)) != NULL)
    cout << "Found " << it << endl;
  else cout << "No key 15\n";
  tree.print();
  if ((it = tree.remove(20)) != NULL)
    cout << "Removed " << it << endl;
  else cout << "No key 20\n";
  cout << "Now, insert 20\n";
  tree.insert(20, new Int(20));
  tree.print();
  if ((it = tree.remove(20)) != NULL)
    cout << "Removed " << it << endl;
  else cout << "No key 20\n";
  tree.print();
  tree.insert(700, new Int(700));
  cout << "Size: " << tree.size() << "\n";
  tree.insert(350, new Int(350));
  tree.insert(201, new Int(201));
  tree.insert(170, new Int(170));
  tree.insert(151, new Int(151));
  tree.insert(190, new Int(190));
  tree.insert(1000, new Int(1000));
  tree.insert(900, new Int(900));
  tree.insert(950, new Int(950));
  tree.insert(10, new Int(10));
  tree.print();
  if ((it = tree.find(1000)) != NULL)
    cout << "Found " << it << endl;
  else cout << "No key 1000\n";
  if ((it = tree.find(999)) != NULL)
    cout << "Found " << it << endl;
  else cout << "No key 999\n";
  if ((it = tree.find(20)) != NULL)
    cout << "Found " << it << endl;
  else cout << "No key 20\n";

  cout << "Now do some delete tests.\n";
  tree.print();
  if ((it = tree.remove(15)) != NULL)
    cout << "Removed " << it << endl;
  else cout << "No key 15\n";
  tree.print();
  if ((it = tree.remove(151)) != NULL)
    cout << "Removed " << it << endl;
  else cout << "No key 151\n";
  tree.print();
  if ((it = tree.remove(151)) != NULL)
    cout << "Removed " << it << endl;
  else cout << "No key 151\n";
  if ((it = tree.remove(700)) != NULL)
    cout << "Removed " << it << endl;
  else cout << "No key 700\n";
  tree.print();
  tree.clear();
  tree.print();
  cout << "Size: " << tree.size() << "\n";

  cout << "Finally, test iterator\n";
  tree.insert(3500, new Int(3500));
  tree.insert(2010, new Int(2010));
  tree.insert(1700, new Int(1700));
  tree.insert(1510, new Int(1510));
  tree.insert(1900, new Int(1900));
  tree.insert(10000, new Int(10000));
  tree.insert(9000, new Int(9000));
  tree.insert(9500, new Int(9500));
  tree.insert(100, new Int(100));
  tree.print();
  cout << "Start:\n";
  Int* temp;
  while (tree.size() > 0) {
    temp = tree.removeAny();
    cout << temp << " ";
  }
  cout << "\n\n";
}


/* Heap Test -------------------------------------------------------------------
 */

// Test out the heap implementation -- with max heap
void HeapTest(int nodes) {
  int i, j;
  int n = nodes;
  Int* A[20];
  Int* B[20];
  Int C[10] = {73, 6, 57, 88, 60, 34, 83, 72, 48, 85};
  Heap1<Int*, maxIntsCompare> BH(B, 0, 20);
  Heap1<Int, maxIntCompare> Test(C, 10, 10);

  /*if (argc != 2) {
    cout << "Usage: heap <heapsize>\n";
    exit(-1);
  }

  n = atoi(argv[1]);*/
  
  if (n > 20) {
    cout << "heap size " << n << " too big.\n";
    exit(-1);
  }

  Randomize();

  for (i=0; i<n; i++)
    A[i] = new Int(i);

  permute(A, n);

  cout << "Initial values:\n";
  for (i=0; i<n; i++) {
    cout << A[i] << "  ";
    if (i == 9) cout << "\n";
  }
  cout << "\n\n";

  for (i=0; i<n; i++)
    BH.insert(A[i]);

  for (i=0; i<BH.size(); i++) {
    cout << B[i] << "  ";
    if (i == 9) cout << "\n";
  }
  cout << "\n\n";

  Heap1<Int*, maxIntsCompare> AH(A, n, 20);
  Int* AHval = NULL;

  for (i=0; i<AH.size(); i++) {
    cout << A[i] << "  ";
    if (i == 9) cout << "\n";
  }
  cout << "\n\n";

  AHval = AH.removefirst();
  cout << "Max value: " << AHval << "\n";

  for (i=0; i<AH.size(); i++) {
    cout << A[i] << "  ";
    if (i == 9) cout << "\n";
  }
  cout << "\n\n";

  AHval = AH.removefirst();
  cout << "Max value: " << AHval << "\n";

  for (i=0; i<AH.size(); i++) {
    cout << A[i] << "  ";
    if (i == 9) cout << "\n";
  }
  cout << "\n\n";

  AHval = AH.remove(2);
  cout << "Remove value: " << AHval << "\n";

  for (i=0; i<AH.size(); i++) {
    cout << A[i] << "  ";
    if (i == 9) cout << "\n";
  }
  cout << "\n";

  for (i=0; i<10; i++)
    cout << C[i] << "  ";
  cout << "\n";

  Int Testval;
  for (j=0; j<10; j++) {
    Testval = Test.removefirst();
    for (i=0; i<10; i++)
      cout << C[i] << "  ";
    cout << "\n\n";
  }
}

// Test program for checking syntax of a hash table-based dictionary

void HashTest() {
  hashdict<int, Int*> dict(100, -1);
  Int* val;

  dict.insert(10, new Int(10));
  if ((val = dict.find(10)) != NULL)
    cout << "Found value " << val << " to match key value 10\n";
  else
    cout << "Nothing found to match key value 10\n";

  hashdict<char*, char*> Strdict(100, (char*)"");
  char* str;

  Strdict.insert((char*)"hello", (char*)"hello");
  if ((str = Strdict.find((char*)"hello")) != NULL)
    cout << "Found value " << str << " to match key value hello\n\n";
  else
    cout << "Nothing found to match key value hello\n\n";
}

#endif	/* b00_DSTEST_H */

