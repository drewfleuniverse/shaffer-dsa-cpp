// ALiu Edit: The class Link of linkFL.h is renamed as Link2
//            The class LList of dlist.h is renamed as LList2
// Shaffer's Source code: linkFL.h dlist.h


#ifndef b04_LIST3_H
#define	b04_LIST3_H
#include "a01_book.h"
#include "b01_ListADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Singly Linked-List, with Freelist Support -----------------------------------
 * new:     Theta(1) 
 * delete:  Theta(1)
 */

// Singly linked list node with freelist support
template <typename E> class Link2 {
private:
  static Link2<E>* freelist; // Reference to freelist head
public:
  E element;                // Value for this node
  Link2* next;                  // Point to next node in list

  // Constructors
  Link2(const E& elemval, Link2* nextval =NULL)
    { element = elemval;  next = nextval; }
  Link2(Link2* nextval =NULL) { next = nextval; }

  void* operator new(size_t) {  // Overloaded new operator
    if (freelist == NULL) return ::new Link2; // Create space
    Link2<E>* temp = freelist; // Can take from freelist
    freelist = freelist->next;
    return temp;                 // Return the link
  }

  // Overloaded delete operator
  void operator delete(void* ptr) {
    ((Link2<E>*)ptr)->next = freelist; // Put on freelist
    freelist = (Link2<E>*)ptr;
  }
};

// The freelist head pointer is actually created here
template <typename E>
Link2<E>* Link2<E>::freelist = NULL;

// This is the declaration for LList. It is split into two parts
// because it is too big to fit on one book page
// Linked list implementation
template <typename E> class LList2: public List<E> {
private:
  Link2<E>* head;       // Pointer to list header
  Link2<E>* tail;       // Pointer to last element
  Link2<E>* curr;       // Access to current element
  int cnt;    	       // Size of list

  void init() {        // Intialization helper method
    curr = tail = head = new Link2<E>;
    cnt = 0;
  }

  void removeall() {   // Return link nodes to free store
    while(head != NULL) {
      curr = head;
      head = head->next;
      delete curr;
    }
  }

public:
  LList2(int size=defaultSize) { init(); }    // Constructor
  ~LList2() { removeall(); }                   // Destructor
  void print() const;                // Print list contents
  void clear() { removeall(); init(); }       // Clear list

  // Insert "it" at current position
  void insert(const E& it) {
    curr->next = new Link2<E>(it, curr->next);  
    if (tail == curr) tail = curr->next;  // New tail
    cnt++;
  }

  void append(const E& it) { // Append "it" to list
    tail = tail->next = new Link2<E>(it, NULL);
    cnt++;
  }

  // Remove and return current element
  E remove() {
    Assert(curr->next != NULL, "No element");
    E it = curr->next->element;      // Remember value
    Link2<E>* ltemp = curr->next;     // Remember link node
    if (tail == curr->next) tail = curr; // Reset tail
    curr->next = curr->next->next;   // Remove from list
    delete ltemp;                    // Reclaim space
    cnt--;			     // Decrement the count
    return it;
  }

  void moveToStart() // Place curr at list start
    { curr = head; }

  void moveToEnd()   // Place curr at list end
    { curr = tail; }

  // Move curr one step left; no change if already at front
  void prev() {
    if (curr == head) return;       // No previous element
    Link2<E>* temp = head;
    // March down list until we find the previous element
    while (temp->next!=curr) temp=temp->next;
    curr = temp;
  }

  // Move curr one step right; no change if already at end
  void next()
    { if (curr != tail) curr = curr->next; }

  int length() const  { return cnt; } // Return length

  // Return the position of the current element
  int currPos() const {
    Link2<E>* temp = head;
    int i;
    for (i=0; curr != temp; i++)
      temp = temp->next;
    return i;
  }

  // Move down list to "pos" position
  void moveToPos(int pos) {
    Assert ((pos>=0)&&(pos<=cnt), "Position out of range");
    curr = head;
    for(int i=0; i<pos; i++) curr = curr->next;
  }

  const E& getValue() const { // Return current element
    Assert(curr->next != NULL, "No value");
    return curr->next->element;
  }
};

#endif	/* b04_LIST3_H */

