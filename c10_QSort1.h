// ALiu Edit: The template functions below are merged into template class
//            QSort1.
//            The function main is removed.
// Shaffer's Source code: qsort1.cpp


#ifndef b10_QSORT1_H
#define	b10_QSORT1_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Quick Sort ------------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(nlog(n))
 * Average:   Theta(nlog(n)); Omicron(n), after certain optimization
 * Worst:     Theta(n^2)
 */

template <typename E, typename Comp>
class QSort1 : public SortADT<E, Comp> {
public:
  // Basic Quicksort version

  // Simple findpivot: Pick middle value in array
  inline int findpivot(E A[], int i, int j)
    { return (i+j)/2; }

  // Partition the array
  inline int partition(E A[], int l, int r, E& pivot) {
    do {             // Move the bounds inward until they meet
      while (Comp::prior(A[++l], pivot));  // Move l right and
      while ((l < r) && Comp::prior(pivot, A[--r])); // r left
      swap(A, l, r);              // Swap out-of-place values
    } while (l < r);              // Stop when they cross
    return l;      // Return first position in right partition
  }

  // qsort core function: Basic qsort
  void qsort1(E A[], int i, int j) { // Quicksort
    if (j <= i) return; // Don't sort 0 or 1 element
    int pivotindex = findpivot(A, i, j);
    swap(A, pivotindex, j);    // Put pivot at end
    // k will be the first position in the right subarray
    int k = partition(A, i-1, j, A[j]);
    swap(A, k, j);             // Put pivot in place
    qsort1(A, i, k-1);
    qsort1(A, k+1, j);
  }

  void sort(E* array, int n) {
    qsort1(array, 0, n-1);
  }
};

#endif	/* b10_QSORT1_H */

