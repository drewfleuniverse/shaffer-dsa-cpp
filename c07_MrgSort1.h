// ALiu Edit: The template functions below are merged into template class
//            MrgSort1.
//            The function main is removed.
// Shaffer's Source code: mrgsort1.cpp

#ifndef b07_MRGSORT1_H
#define	b07_MRGSORT1_H
#include "a01_book.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


/* Merge Sort ------------------------------------------------------------------
 * Time Complexity
 * Best:      Theta(nlog(n))
 * Average:   Theta(nlog(n))
 * Worst:     Theta(nlog(n))
 */

template <typename E, typename Comp>
class MrgSort1 : public SortADT<E, Comp> {
public:
  // Basic mergesort implementation
  void mergesort1(E A[], E temp[], int left, int right) {
    if (left == right) return;        // List of one element
    int mid = (left+right)/2;
    mergesort1(A, temp, left, mid);
    mergesort1(A, temp, mid+1, right);
    for (int i=left; i<=right; i++)   // Copy subarray to temp
      temp[i] = A[i];
    // Do the merge operation back to A
    int i1 = left; int i2 = mid + 1;
    for (int curr=left; curr<=right; curr++) {
      if (i1 == mid+1)      // Left sublist exhausted
        A[curr] = temp[i2++];
      else if (i2 > right)  // Right sublist exhausted
        A[curr] = temp[i1++];
      else if (Comp::prior(temp[i1], temp[i2]))
        A[curr] = temp[i1++];
      else A[curr] = temp[i2++];
    }
  }

  void sort(E* array, int n) {
    static E* temp = NULL;
    if (temp == NULL) temp = new E[n];  // Declare temp array
    mergesort1(array, temp, 0, n-1);
  }
};

#endif	/* b07_MRGSORT1_H */

