// ALiu Edit: The template functions below are merged into template class
//            HeapSort2.
//            The function main is removed.
// Shaffer's Source code: heapsort2.cpp


#ifndef c15_HEAPSORT2_H
#define	c15_HEAPSORT2_H
#include "a01_book.h"
#include "b17_Heap2.h"
#include "c01_SortADT.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


template <typename E, typename Comp>
class HeapSort2 : public SortADT<E, Comp> {
public:
  // This heapsort uses a modified siftdown operation that
  // always promotes the larger child regardless of root value.

  // Heapsort implementation
  void heapsort2(E A[], int n) { // Heapsort
    E maxval;
    Heap2<E,Comp> H(A, n, n);    // Build the heap
    for (int i=0; i<n; i++)        // Now sort
      maxval = H.removefirst();    // Place maxval at end
  }

  void sort(E* array, int n) {
   heapsort2(array, n);
  }
};

#endif	/* c15_HEAPSORT2_H */

