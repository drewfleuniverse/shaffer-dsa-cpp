// ALiu Edit: The function HashTest is modified from the function syntaxCheck
//            of hashdict.cpp
//            The function BSearchTest is modified from the function main of 
//            bsearch.cpp
// Shaffer's Source code: hashdict.cpp bsearch.cpp


#ifndef d00_SEARCHTEST_H
#define	d00_SEARCHTEST_H
#include "a01_book.h"
#include "d01_BSearch.h"
// From the software distribution accompanying the textbook
// "A Practical Introduction to Data Structures and Algorithm Analysis,
// Third Edition (C++)" by Clifford A. Shaffer.
// Source code Copyright (C) 2007-2011 by Clifford A. Shaffer.


// Test program to test out both the specific and the generic bsearch
// algorithm.
void BSearchTest(int numvals, int key) {
  int i, num, K, result;
  int* A;
  Int* B;
  Int** C;

  num = numvals;
  K = key;

  // Create and initialize array
  A = new int[numvals];
  B = new Int[numvals];
  C = new Int*[numvals];
	
  for (i=0; i<numvals; i++)
    A[i] = i;

  // Call to specialized implementation (integer only)
  result = binary(A, numvals, K);
  if (result == numvals)
    cout << "Binary search was unsuccessful\n";
  else
    cout << "Binary search returns " << result << "\n";

  for (i=0; i<numvals; i++)
    { A[i] = i; B[i] = i; C[i] = new Int(i); }

  // Call to generalized template form
  result = binaryt<int, int, intintCompare, getintKey>(A, numvals, K);
  if (result == numvals)
    cout << "Binary search was unsuccessful\n";
  else
    cout << "Binary search returns " << result << "\n";

  result = binaryt<int, Int, intintCompare, getIntKey>(B, numvals, K);
  if (result == numvals)
    cout << "Binary search was unsuccessful\n";
  else
    cout << "Binary search returns " << result << "\n";

  // Call to generalized template form
  result = binaryt<int, Int*, intintCompare, getIntsKey>(C, numvals, K);
  if (result == numvals)
    cout << "Binary search was unsuccessful\n\n";
  else
    cout << "Binary search returns " << result << "\n\n";
}

#endif	/* d00_SEARCHTEST_H */

